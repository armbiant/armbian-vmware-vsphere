
## 0.2.0 [05-23-2022]

* Minor/adapt 2059:: Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!3

---

## 0.1.3 [08-02-2021]

- Task- listLibraries 
  - Change- Add /com/vmware
- Task- listLibraryItems
  - Change- Add /com/vmware
  - Change- library_id
- Task- getLibraryItemDetails
  - Change- Add /com/vmware
  - Change- Add id: before query

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!2

---

## 0.1.2 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!1

---

## 0.1.1 [01-04-2021]

- Initial Commit

See commit f382a1f

---
